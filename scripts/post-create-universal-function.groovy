import com.atlassian.jira.bc.issue.properties.IssuePropertyService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.entity.property.EntityProperty
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.issuetype.IssueType
import com.atlassian.jira.user.ApplicationUser

// MutableIssue issue = null // comment out this line before deploy

/**
 * Note: changing issue type is potentially problematic,
 * you should make sure that new issue type shares the same workflow
 * (at least workflow states) and field config
 */
def setIssueType(MutableIssue issue, String typeName) {
    IssueType newIssueType = ComponentAccessor.issueTypeSchemeManager
            .getIssueTypesForProject(issue.projectObject)
            .find {it.name == typeName }
    if (newIssueType) {
        issue.setIssueTypeObject(newIssueType)
    } else {
        log.error("Couldn't find issue type: " + typeName)
    }
}

def setCustomField(MutableIssue issue, String fieldName, Object value) {
    CustomField field = ComponentAccessor.customFieldManager.getCustomFieldObjectByName(fieldName)
    if (field != null) {
        issue.setCustomFieldValue(field, value)
    } else {
        log.error("Couldn't find custom field: " + fieldName)
    }
}

CustomField getCustomField(String name) {
    return ComponentAccessor.customFieldManager.getCustomFieldObjectByName(name)
}

Object getCustomFieldValue(Issue issue, String fieldName) {
    CustomField field = getCustomField(fieldName)
    if (field != null) {
        return issue.getCustomFieldValue(field)
    } else {
        return null
    }
}

def setRequestType(MutableIssue issue, String rtDbString) {
    CustomField rtField = getCustomField("Customer Request Type")
    Object value = rtField.customFieldType.getSingularObjectFromString(rtDbString)
    issue.setCustomFieldValue(rtField, value)
}

def processSummary(String summary) {
    int idx = summary.indexOf("Facility access request")
    if (idx < 0) { // not found
        return summary
    }
    return summary.substring(idx)
}

def getUser(String name) {
    return ComponentAccessor.userManager.getUserByName(name)
}

def getComponent(Issue issue, String name) {
    return ComponentAccessor.projectComponentManager.findByComponentName(issue.projectId, name)
}

def processIssue(MutableIssue issue) {
    log.warn("setting new summary:" + processSummary(issue.summary))

    setIssueType(issue, "Access")
    issue.setSummary(processSummary(issue.summary))
    issue.component = [getComponent(issue, "Identity")]
    issue.setAssignee(getUser('divina'))
    setCustomField(issue, "Request participants", [getUser('divina')])

    Option calpendo = ComponentAccessor.optionsManager.findByOptionId(11001)
    setCustomField(issue, "Scope", [calpendo])

    // hd/system-access ... "Get access to a system"
    setRequestType(issue,"hd/system-access")

    ApplicationUser adminUser = getUser('divina')
    ComponentAccessor.issueManager.updateIssue(adminUser, issue, EventDispatchOption.ISSUE_UPDATED, false)
}

String getChannel(Issue issue) {
    def adminUser = getUser('divina')
    IssuePropertyService issuePropertyService = ComponentAccessor.getComponentOfType(IssuePropertyService.class)
    EntityProperty entityProperty = issuePropertyService.getProperty(adminUser, issue.id, "request.channel.type")?.getEntityProperty()?.getOrNull()
    return entityProperty?.getValue()
}

boolean isChannelEmail(Issue issue) {
    String val = (String)getChannel(issue)
    log.warn("channel: " + val)
    return val?.contains("email")
}

boolean isIssueSourceEmail(Issue issue) {
    Option val = (Option)getCustomFieldValue(issue, "Issue source")
    String label = val?.value
    log.warn("Issue source: " + label)
    return label?.contains("E-mail")
}

boolean isIssueFromEmail(Issue issue) {
    return isChannelEmail(issue) || isIssueSourceEmail(issue) 
}

boolean isRelevantIssue(MutableIssue issue) {
    log.warn("Issue " + issue.key + " Id: " + issue.id )
    log.warn("Summary: " + issue.summary)

    boolean res = isIssueFromEmail(issue) && issue.summary?.contains("Facility access request")
    
    log.warn("relevant:" + res)
    return res
}

if (isRelevantIssue(issue)) {
    processIssue(issue)
}

