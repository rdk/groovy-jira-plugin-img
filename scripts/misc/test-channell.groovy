import com.atlassian.jira.bc.issue.properties.IssuePropertyService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.fields.CustomField

//=================================================================//
class gg {static String msg = ""}
def print(String s) { log.warn s; gg.msg += s }
def println(String s) { print s + "\n" }
//=================================================================//

Issue issue = ComponentAccessor.issueManager.getIssueByCurrentKey("IT-19856")
//CustomField rtField = ComponentAccessor.customFieldManager.getCustomFieldObjectByName("Channel")
//
//Object value = issue.getCustomFieldValue(rtField)
//String str = rtField.customFieldType.getStringFromSingularObject(value)

def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
IssuePropertyService issuePropertyService = ComponentAccessor.getComponentOfType(IssuePropertyService.class)
def option = issuePropertyService.getProperty(user, issue.id, "request.channel.type")?.getEntityProperty()
Object ep = option?.getOrNull()
Object val = ep?.value

println "Value: [$val] "
println "Value Class: [${val.getClass().simpleName}] "
println "Object: [$ep] "
println "Class: [${ep.getClass().simpleName}] "
println "DONE"

//=================================================================//
return gg.msg.replaceAll("\n", "<br>")