import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.fields.CustomField

//=================================================================//
class g {static String msg = ""}
def print(String s) { log.warn s; g.msg += s }
def println(String s) { print s + "\n" }
//=================================================================//

Issue issue = ComponentAccessor.issueManager.getIssueByCurrentKey("IT-19831")
CustomField rtField = ComponentAccessor.customFieldManager.getCustomFieldObjectByName("Customer Request Type")

Object value = issue.getCustomFieldValue(rtField)
String str = rtField.customFieldType.getStringFromSingularObject(value)

println "Object: [$value] "
println "String: [$str] "
println "DONE"

//=================================================================//
return g.msg.replaceAll("\n", "<br>")