import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.security.IssueSecurityLevel
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager
import com.atlassian.jira.user.ApplicationUser

/**
 * - pokud jsou mezi komponentami EIS a Booking -> SL: Standard + EIS + Booking
 * - pokud je mezi komponentami Booking -> SL: Standard + Booking
 * - pokud je mezi komponentami EIS -> SL: Standard + EIS
 * - pokud mezi komponentami neni EIS ani Booking -> SL: Standard
 *
 *
 * @param issue
 * @param levelName
 * @return
 */

IssueSecurityLevel getSecurityLevel(Issue issue, String levelName) {
    Long securitySchemeId = ComponentAccessor.getComponent(IssueSecuritySchemeManager).getSchemeIdFor(issue.projectObject)
    return ComponentAccessor.issueSecurityLevelManager.getSecurityLevelByNameAndSchema(levelName, securitySchemeId)
}

def setSecurityLevel(MutableIssue issue, String levelName) {
    ApplicationUser actor = ComponentAccessor.jiraAuthenticationContext.loggedInUser

    issue.setSecurityLevelId(getSecurityLevel(issue, levelName).id)
    ComponentAccessor.issueManager.updateIssue(actor, issue, EventDispatchOption.ISSUE_UPDATED, false)
}


MutableIssue issue = event.issue

// these linas are equal expressions
//def components = issue.components.collect { ProjectComponent it -> it.name }
//def components = issue.components.collect { def it -> it.name }
//def components = issue.components.collect { it.name }
def components = issue.components*.name

if (components.containsAll(["EIS", "Booking"])) {
    setSecurityLevel(issue, "SL: Standard + EIS + Booking")
} else if ("EIS" in components) {
    setSecurityLevel(issue, "SL: Standard + EIS")
} else if ("Booking" in components) {
    setSecurityLevel(issue, "SL: Standard + Booking")
} else {
    setSecurityLevel(issue, "SL: Standard")
}

// alternativa

def mapping = [
        [ comps: ["EIS", "Booking"], level: "SL: Standard + EIS + Booking"],
        [ comps: ["EIS"]           , level: "SL: Standard + EIS"          ],
        [ comps: ["Booking"]       , level: "SL: Standard + Booking"      ],
        [ comps: []                , level: "SL: Standard"                ]
]

for (def entry : mapping) {
    if (components.containsAll(entry.comps)) {
        setSecurityLevel(issue, entry.level)
        break
    }
}






