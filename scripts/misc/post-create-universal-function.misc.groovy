import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.issuetype.IssueType
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.servicedesk.api.portal.PortalService
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.onresolve.scriptrunner.runner.customisers.WithPlugin

@WithPlugin("com.atlassian.servicedesk")

@PluginModule
RequestTypeService requestTypeService

@PluginModule
PortalService portalService

def serviceDeskIssueKey = "SD-1"
def serviceDeskIssue = ComponentAccessor.getIssueManager().getIssueByCurrentKey(serviceDeskIssueKey)
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def requestTypes = requestTypeService.getAllRequestTypes(user)

if (requestTypes.isLeft()) {
    log.error "${requestTypes.left().get()}"
    return
}

if (!serviceDeskIssue)
    return

def portalRequest = portalService.getPortalForProject(user, serviceDeskIssue.projectObject)

if (portalRequest.isLeft()) {
    log.debug "Error ${portalRequest.left().get()}"
    return
}

def portal = portalRequest.right().get()

return requestTypes?.right()?.get()?.findAll {it.issueTypeId == serviceDeskIssue.issueTypeId as Long &amp;&amp; it.portalId == portal.id}?.collect {
    "Request type: ${it.name} | ID: ${it.id} | Description: ${it.description}}"
}?.join("&lt;br&gt;")


import com.atlassian.jira.component.ComponentAccessor

// In order to find the key of it select an request type for an issue and then navigate to
// http://&lt;baseUrl&gt;/rest/api/2/issue/&lt;issueKey&gt;?expand=renderedFields,editmeta
// and next to the id of the Customer Request Type custom field (see below in log) you will find it
def REPORT_SYSTEM_PROBLEM_KEY = "sdp/systemproblem"

def customFieldManager = ComponentAccessor.getCustomFieldManager()
def tgtField = customFieldManager.getCustomFieldObjectByName("Customer Request Type")

// get the id of the cf in the logs in order to use it for finding the key of the Customer Request Type
log.debug("Customer Request Type Custom field Id: ${tgtField.getId()}")

def requestType = tgtField.getCustomFieldType().getSingularObjectFromString(REPORT_SYSTEM_PROBLEM_KEY)
issue.setCustomFieldValue(tgtField, requestType)



MutableIssue issue = null // comment out this line before deploy


/**
 * Note: changing issue type is potentially problematic,
 * you should make sure that new issue type shares the same workflow
 * (at least workflow states) and field config
 */
def setIssueType(MutableIssue issue, String typeName) {
    IssueType newIssueType = ComponentAccessor.issueTypeSchemeManager
            .getIssueTypesForProject(issue.projectObject)
            .find{it.name == typeName }
    if (newIssueType) {
        issue.setIssueTypeObject(newIssueType)
    }
}

def setCustomField(MutableIssue issue, String fieldName, Object value) {
    CustomField field = ComponentAccessor.customFieldManager.getCustomFieldObjectByName(fieldName)
    if (field != null) {
        issue.setCustomFieldValue(field, value)
    }
}

def processSummary(String summary) {
    int idx = summary.indexOf("Facility access request")
    if (idx < 0) { // not found
        return summary
    }
    return summary.substring(idx)
}


setIssueType(issue, "Access")
issue.component = [ComponentAccessor.projectComponentManager.findByComponentName(issue.projectId, "Identity")]
issue.setAssignee(ComponentAccessor.userManager.getUserByName('divina'))
setCustomField(issue, "Request participant", ComponentAccessor.userManager.getUserByName('divina'))
issue.setSummary(processSummary(issue.summary))

CustomField rtField = ComponentAccessor.customFieldManager.getCustomFieldObjectByName("Customer Request Type")
Object rtValue = rtField.customFieldType.getSingularObjectFromString("Incident")
issue.setCustomFieldValue(rtField, )