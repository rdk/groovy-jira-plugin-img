import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.IssueEvent
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.security.IssueSecurityLevel
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager
import com.atlassian.jira.ofbiz.OfBizDelegator
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.util.collect.MapBuilder
import org.ofbiz.core.entity.GenericValue

/**
 2/ custom listener pro by mel nastavit security level pri kazde zmene Scope podle nasledujiciho mapovani

 Scope                    Security Level

 Calpendo, EIS Magion     All Support
 Calpendo                 Booking Support
 EIS Magion               EIS Support
 (prazdny)                Standard Support
 *
 *
 * @param issue
 * @param levelName
 * @return
 */

List<GenericValue> getChangedItems(IssueEvent event) {
    if (event == null || event.getChangeLog() == null) return Collections.emptyList()

    GenericValue changelog = event.getChangeLog()
    Long changeGroupId = changelog.getLong("id")
    OfBizDelegator delegator = ComponentAccessor.getOfBizDelegator()
    List<GenericValue> changeItems = delegator
            .findByAnd("ChangeItem", MapBuilder.build("group", changeGroupId))
    return changeItems
}

boolean isChangedField(IssueEvent event, String fieldName) {
    List<GenericValue> changeItems = getChangedItems(event)

    log.debug("isChangedField: event changeItems.size = " + changeItems.size())
    log.debug("isChangedField: event fieldName = " + fieldName)

    for (GenericValue changeItem : changeItems) {
        String changedFieldName = String.valueOf(changeItem.get("field"))

        log.debug("isChangedField: event changedItem = " + changedFieldName)
        //test status changed
        if (fieldName.equals(changedFieldName)) {
            return true
        }
    }
    return false
}

IssueSecurityLevel getSecurityLevel(Issue issue, String levelName) {
    Long securitySchemeId = ComponentAccessor.getComponent(IssueSecuritySchemeManager).getSchemeIdFor(issue.projectObject)
    return ComponentAccessor.issueSecurityLevelManager.getSecurityLevelByNameAndSchema(levelName, securitySchemeId)
}

def setSecurityLevel(MutableIssue issue, String levelName) {
    ApplicationUser actor = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    IssueSecurityLevel level = getSecurityLevel(issue, levelName)

    if (level != null) {
        issue.setSecurityLevelId(getSecurityLevel(issue, levelName).id)
        ComponentAccessor.issueManager.updateIssue(actor, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
    } else {
        log.error("Security level not found: " + levelName)
    }
}

List<String> getScopes(Issue issue) {
    CustomField scopeField = ComponentAccessor.customFieldManager.getCustomFieldObjectByName("Scope")
    return ((Collection<Option>)issue.getCustomFieldValue(scopeField))*.value
}

void processEvent(IssueEvent event) {
    def mapping = [
            [ scopes: ["Calpendo", "EIS Magion"], level: "All Support"],
            [ scopes: ["Calpendo"]              , level: "Booking Support" ],
            [ scopes: ["EIS Magion"]            , level: "EIS Support"      ],
            [ scopes: []                        , level: "Standard Support" ]
    ]

    MutableIssue issue = event.issue

    def scopes = getScopes(issue)
    for (def entry : mapping) {
        if (scopes.containsAll(entry.scopes)) {
            setSecurityLevel(issue, entry.level)
            break
        }
    }
}

if (isChangedField(event, "Scope")) {
    processEvent(event)
}






