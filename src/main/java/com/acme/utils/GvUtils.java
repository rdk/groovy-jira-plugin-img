package com.acme.utils;

import org.joda.time.DateTime;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.List;

public abstract class GvUtils {
	

	public static DateTime timestampToDateTime(Timestamp timestamp) {
		return timestamp==null ? null : new DateTime(timestamp);
	}
	
	public static Timestamp dateTimeToTimestamp(DateTime dateTime) {
		return dateTime==null ? null : new Timestamp(dateTime.getMillis());
	}
	
	
	public static GenericValue getOneValue(List<GenericValue> gvs) {
		if (gvs==null || gvs.isEmpty()) {
			return null;
		} else {
			if (gvs.size()>1) {
				try {
					throw new IllegalStateException("There are more records in db where at most one is expected! "+gvs.size());
				} catch (Exception e) {
					log.warn(e.getMessage(),e);
				}
			}
			
			return gvs.get(0);
		}
	}
	
//================================================================================================//
	
	private static final Logger log = LoggerFactory.getLogger(GvUtils.class);
	
}
